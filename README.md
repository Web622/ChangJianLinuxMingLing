# 常见Linux命令

- 更改分辨率
  ```
  xrandr --output Virtual1 --mode "1280*800"
  ```


- 运行django项目
  ```
  python manage.py runserver
  python3 manage.py runserver
  ```

- 创建django模型表
  ```
  $ python manage.py migrate    创建表结构
  $ python manage.py makemigrations TestModel   让 Django 知道我们在我们的模型有一些变更
  $ python manage.py migrate TestModel    创建表结构
  ```